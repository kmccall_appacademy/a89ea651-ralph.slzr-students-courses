# ## Student
# * `Student#initialize` should take a first and last name.
# * `Student#name` should return the concatenation of the student's
#   first and last name.
# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.
class Student
  attr_accessor :first_name, :last_name, :courses
  attr_reader :enroll
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    @first_name + ' ' + @last_name
  end

  def new_course
    new_course = Course.new
  end

  def enroll(new_course)
    @courses.each { |course| raise 'error' if course.conflicts_with?(new_course) }
    @courses << new_course unless @courses.include?(new_course)
    new_course.students << self unless new_course.students.include?(self)
  end

 # Student#course_load returns a hash of department names pointing to # of credits
  def course_load
    course_hash = {}
    @courses.each do |course|
      if course_hash.has_key?(course.department)
        course_hash[course.department] += course.credits
      else
        course_hash[course.department] = course.credits
      end
    end
    course_hash
  end

end
